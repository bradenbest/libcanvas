var Canvas = (function(){

    function Color(_r, _g, _b){//{{{
        var r,
            g,
            b;

        function init(){//{{{
            r = _r || 0;
            g = _g || 0;
            b = _b || 0;
        }//}}}

        init();
        this.r = r;
        this.g = g;
        this.b = b;
    }//}}}

    function Keyboard(){//{{{
        //private
        var table = {};

        //public
        function update(event){//{{{
            table[event.keyCode] = event.type == "keydown";
        }//}}}

        function keyDown(key){//{{{
            return !!table[key];
        }//}}}
        
        this.update  = update;
        this.keyDown = keyDown;
    }//}}}

    function Mouse(){//{{{
        //private
        var left   = false,
            middle = false,
            right  = false,
            x = 0,
            y = 0;

        function updateClick(event){//{{{
            switch(event.button){
                case 0:
                    left = event.type == "mousedown";
                    break;
                case 1:
                    middle = event.type == "mousedown";
                    break;
                case 2:
                    right = event.type == "mousedown";
                    break;
                default:
                    left   = false;
                    middle = false;
                    right  = false;
                    break;
            }
        }//}}}

        function getCoords(event, canvas){//{{{
            var x = event.clientX || 0,
                y = event.clientY || 0;

            return [
                x - canvas.getBoundingClientRect().left,
                y - canvas.getBoundingClientRect().top
            ];
        }//}}}

        function updateMove(event, canvas){//{{{
            var coords = getCoords(event, canvas);

            x = coords[0];
            y = coords[1];
        }//}}}

        //public
        function update(event, canvas){//{{{
            switch(event.type){
                case "mousedown":
                case "mouseup":
                    updateClick(event);
                    break;

                case "mousemove":
                    updateMove(event, canvas);
                    break;
            }
        }//}}}

        function leftDown(){//{{{
            return !!left;
        }//}}}

        function middleDown(){//{{{
            return !!middle;
        }//}}}

        function rightDown(){//{{{
            return !!right;
        }//}}}

        function getPosition(){//{{{
            return [x, y];
        }//}}}

        this.update      = update;
        this.leftDown    = leftDown;
        this.middleDown  = middleDown;
        this.rightDown   = rightDown;
        this.getPosition = getPosition;
    }//}}}

    var colorTable = {//{{{
        red    : new Color(0xcc, 0x33, 0x33),
        green  : new Color(0x33, 0xcc, 0x33),
        blue   : new Color(0x33, 0x33, 0xcc),
        yellow : new Color(0xdd, 0xdd, 0x33),
        violet : new Color(0xcc, 0x33, 0xcc),
        pink   : new Color(0xff, 0xbb, 0xbb),
        cyan   : new Color(0x11, 0xcc, 0xee),
        black  : new Color(0x33, 0x33, 0x33),
        white  : new Color(0xee, 0xee, 0xee),
    };//}}}

    var keyTable = {//{{{
    // 00 - 0F {{{
        "NUL"         : 0x00,
        "Unused_01"   : 0x01,
        "Unused_02"   : 0x02,
        "Unused_03"   : 0x03,
        "Unused_04"   : 0x04,
        "Unused_05"   : 0x05,
        "Unused_06"   : 0x06,
        "Unused_07"   : 0x07,
        "Backspace"   : 0x08,
        "Tab"         : 0x09,
        "Unused_0A"   : 0x0A,
        "Unused_0B"   : 0x0B,
        "Unused_0C"   : 0x0C,
        "Enter"       : 0x0D, /* Return             */
        "Unused_0E"   : 0x0E,
        "Unused_0F"   : 0x0F,
    //}}}
    // 10 - 1F {{{
        "Shift"       : 0x10,
        "Ctrl"        : 0x11, /* Control            */
        "Alt"         : 0x12,
        "Pause"       : 0x13, /* Pause/Break        */
        "CapsLock"    : 0x14, /* Caps Lock          */
        "Unused_15"   : 0x15,
        "Unused_16"   : 0x16,
        "Unused_17"   : 0x17,
        "Unused_18"   : 0x18,
        "Unused_19"   : 0x19,
        "Unused_1A"   : 0x1A,
        "Escape"      : 0x1B, /* Esc                */
        "Unused_1C"   : 0x1C,
        "Unused_1D"   : 0x1D,
        "Unused_1E"   : 0x1E,
        "Unused_1F"   : 0x1F,
    //}}}
    // 20 - 2F {{{
        "Space"       : 0x20,
        "PageUp"      : 0x21, /* Page Up            */
        "PageDown"    : 0x22, /* Page Down          */
        "End"         : 0x23,
        "Home"        : 0x24,
        "Left"        : 0x25, /* Arrow Keys         */
        "Up"          : 0x26,
        "Right"       : 0x27,
        "Down"        : 0x28,
        "Unused_29"   : 0x29,
        "Unused_2A"   : 0x2A,
        "Unused_2B"   : 0x2B,
        "Unused_2C"   : 0x2C,
        "Insert"      : 0x2D,
        "Delete"      : 0x2E,
        "Unused_2F"   : 0x2F,
    //}}}
    // 30 - 3F {{{
        "0"           : 0x30, /* Number Row         */
        "1"           : 0x31,
        "2"           : 0x32,
        "3"           : 0x33,
        "4"           : 0x34,
        "5"           : 0x35,
        "6"           : 0x36,
        "7"           : 0x37,
        "8"           : 0x38,
        "9"           : 0x39,
        "Unused_3A"   : 0x3A,
        "Unused_3B"   : 0x3B,
        "Unused_3C"   : 0x3C,
        "Unused_3D"   : 0x3D,
        "Unused_3E"   : 0x3E,
        "Unused_3F"   : 0x3F,
    //}}}
    // 40 - 4F {{{
        "Unused_40"   : 0x40,
        "A"           : 0x41, /* A - Z              */
        "B"           : 0x42,
        "C"           : 0x43,
        "D"           : 0x44,
        "E"           : 0x45,
        "F"           : 0x46,
        "G"           : 0x47,
        "H"           : 0x48,
        "I"           : 0x49,
        "J"           : 0x4A,
        "K"           : 0x4B,
        "L"           : 0x4C,
        "M"           : 0x4D,
        "N"           : 0x4E,
        "O"           : 0x4F,
    //}}}
    // 50 - 5F {{{
        "P"           : 0x50,
        "Q"           : 0x51,
        "R"           : 0x52,
        "S"           : 0x53,
        "T"           : 0x54,
        "U"           : 0x55,
        "V"           : 0x56,
        "W"           : 0x57,
        "X"           : 0x58,
        "Y"           : 0x59,
        "Z"           : 0x5A,
        "SuperL"      : 0x5B, /* Aka "Windows Key"  */
        "SuperR"      : 0x5C, /* Also "Meta Key"    */
        "Menu"        : 0x5D, /* Context Menu       */
        "Unused_5E"   : 0x5E,
        "Unused_5F"   : 0x5F,
    //}}}
    // 60 - 6F {{{
        "Num0"        : 0x60, /* Num Pad            */
        "Num1"        : 0x61,
        "Num2"        : 0x62,
        "Num3"        : 0x63,
        "Num4"        : 0x64,
        "Num5"        : 0x65,
        "Num6"        : 0x66,
        "Num7"        : 0x67,
        "Num8"        : 0x68,
        "Num9"        : 0x69,
        "Num*"        : 0x6A, /* Asterisk/Multiply  */
        "Num+"        : 0x6B, /* Plus/Add           */
        "Unused_6C"   : 0x6C,
        "Num-"        : 0x6D, /* Hyphen/Subtract    */
        "Num."        : 0x6E, /* Dot/Period/Decimal */
        "Num/"        : 0x6F, /* Slash              */
    //}}}
    // 70 - 7F {{{
        "F1"          : 0x70, /* Function Keys      */
        "F2"          : 0x71,
        "F3"          : 0x72,
        "F4"          : 0x73,
        "F5"          : 0x74,
        "F6"          : 0x75,
        "F7"          : 0x76,
        "F8"          : 0x77,
        "F9"          : 0x78,
        "F10"         : 0x79,
        "F11"         : 0x7A,
        "F12"         : 0x7B,
        "Unused_7C"   : 0x7C,
        "Unused_7D"   : 0x7D,
        "Unused_7E"   : 0x7E,
        "Unused_7F"   : 0x7F,
    //}}}
    // 80 - 8F {{{
        "Unused_80"   : 0x80,
        "Unused_81"   : 0x81,
        "Unused_82"   : 0x82,
        "Unused_83"   : 0x83,
        "Unused_84"   : 0x84,
        "Unused_85"   : 0x85,
        "Unused_86"   : 0x86,
        "Unused_87"   : 0x87,
        "Unused_88"   : 0x88,
        "Unused_89"   : 0x89,
        "Unused_8A"   : 0x8A,
        "Unused_8B"   : 0x8B,
        "Unused_8C"   : 0x8C,
        "Unused_8D"   : 0x8D,
        "Unused_8E"   : 0x8E,
        "Unused_8F"   : 0x8F,
    //}}}
    // 90 - 9F {{{
        "NumLock"     : 0x90, /* Number Lock        */
        "ScrLock"     : 0x91, /* Scroll Lock        */
        "Unused_92"   : 0x92,
        "Unused_93"   : 0x93,
        "Unused_94"   : 0x94,
        "Unused_95"   : 0x95,
        "Unused_96"   : 0x96,
        "Unused_97"   : 0x97,
        "Unused_98"   : 0x98,
        "Unused_99"   : 0x99,
        "Unused_9A"   : 0x9A,
        "Unused_9B"   : 0x9B,
        "Unused_9C"   : 0x9C,
        "Unused_9D"   : 0x9D,
        "Unused_9E"   : 0x9E,
        "Unused_9F"   : 0x9F,
    //}}}
    // A0 - AF {{{
        "Unused_A0"   : 0xA0,
        "Unused_A1"   : 0xA1,
        "Unused_A2"   : 0xA2,
        "Unused_A3"   : 0xA3,
        "Unused_A4"   : 0xA4,
        "Unused_A5"   : 0xA5,
        "Unused_A6"   : 0xA6,
        "Unused_A7"   : 0xA7,
        "Unused_A8"   : 0xA8,
        "Unused_A9"   : 0xA9,
        "Unused_AA"   : 0xAA,
        "Unused_AB"   : 0xAB,
        "Unused_AC"   : 0xAC,
        "Unused_AD"   : 0xAD,
        "Unused_AE"   : 0xAE,
        "Unused_AF"   : 0xAF,
    //}}}
    // B0 - BF {{{
        "Unused_B0"   : 0xB0,
        "Unused_B1"   : 0xB1,
        "Unused_B2"   : 0xB2,
        "Unused_B3"   : 0xB3,
        "Unused_B4"   : 0xB4,
        "Unused_B5"   : 0xB5,
        "Unused_B6"   : 0xB6,
        "Unused_B7"   : 0xB7,
        "Unused_B8"   : 0xB8,
        "Unused_B9"   : 0xB9,
        "Semicolon"   : 0xBA, /*        ;           */
        "Equals"      : 0xBB, /*        =           */
        "Comma"       : 0xBC, /*        ,           */
        "Hyphen"      : 0xBD, /* Dash   -           */
        "Period"      : 0xBE, /* Dot    .           */
        "Slash"       : 0xBF, /*        /           */
    //}}}
    // C0 - CF {{{
        "Accent"      : 0xC0, /* Grave  `           */
        "Unused_C1"   : 0xC1,
        "Unused_C2"   : 0xC2,
        "Unused_C3"   : 0xC3,
        "Unused_C4"   : 0xC4,
        "Unused_C5"   : 0xC5,
        "Unused_C6"   : 0xC6,
        "Unused_C7"   : 0xC7,
        "Unused_C8"   : 0xC8,
        "Unused_C9"   : 0xC9,
        "Unused_CA"   : 0xCA,
        "Unused_CB"   : 0xCB,
        "Unused_CC"   : 0xCC,
        "Unused_CD"   : 0xCD,
        "Unused_CE"   : 0xCE,
        "Unused_CF"   : 0xCF,
    //}}}
    // D0 - DF {{{
        "Unused_D0"   : 0xD0,
        "Unused_D1"   : 0xD1,
        "Unused_D2"   : 0xD2,
        "Unused_D3"   : 0xD3,
        "Unused_D4"   : 0xD4,
        "Unused_D5"   : 0xD5,
        "Unused_D6"   : 0xD6,
        "Unused_D7"   : 0xD7,
        "Unused_D8"   : 0xD8,
        "Unused_D9"   : 0xD9,
        "Unused_DA"   : 0xDA,
        "OpenSquare"  : 0xDB, /*        [           */
        "BackSlash"   : 0xDC, /*        \           */
        "CloseSquare" : 0xDD, /*        ]           */
        "Apostrophe"  : 0xDE, /*        '           */
        "Unused_DF"   : 0xDF,
    //}}}
    // E0 - EF {{{
        "Unused_E0"   : 0xE0,
        "Unused_E1"   : 0xE1,
        "Unused_E2"   : 0xE2,
        "Unused_E3"   : 0xE3,
        "Unused_E4"   : 0xE4,
        "Unused_E5"   : 0xE5,
        "Unused_E6"   : 0xE6,
        "Unused_E7"   : 0xE7,
        "Unused_E8"   : 0xE8,
        "Unused_E9"   : 0xE9,
        "Unused_EA"   : 0xEA,
        "Unused_EB"   : 0xEB,
        "Unused_EC"   : 0xEC,
        "Unused_ED"   : 0xED,
        "Unused_EE"   : 0xEE,
        "Unused_EF"   : 0xEF,
    //}}}
    // F0 - FF {{{
        "Unused_F0"   : 0xF0,
        "Unused_F1"   : 0xF1,
        "Unused_F2"   : 0xF2,
        "Unused_F3"   : 0xF3,
        "Unused_F4"   : 0xF4,
        "Unused_F5"   : 0xF5,
        "Unused_F6"   : 0xF6,
        "Unused_F7"   : 0xF7,
        "Unused_F8"   : 0xF8,
        "Unused_F9"   : 0xF9,
        "Unused_FA"   : 0xFA,
        "Unused_FB"   : 0xFB,
        "Unused_FC"   : 0xFC,
        "Unused_FD"   : 0xFD,
        "Unused_FE"   : 0xFE,
        "Unused_FF"   : 0xFF,
    //}}}
    };//}}}

    function Canvas2D(canvasElement){//{{{
        // private
        var width,
            height,
            canvas      = canvasElement || null,
            ctx         = null,
            keyboard    = new Keyboard(),
            mouse       = new Mouse(),
            windowWatch = false;

        function createCanvas(){//{{{
            canvas = document.createElement("canvas");
            document.body.appendChild(canvas);
        }//}}}

        function init(){//{{{
            if(!canvas){
                createCanvas();
            }

            setSize(0, 0);
            canvas.tabIndex = 0;
            ctx = canvas.getContext("2d");
        }//}}}

        function colorToRGB(color){//{{{
            var colorObj = parseColor(color);

            return (
                    "rgb(%,%,%)"
                    .replace("%", colorObj.r)
                    .replace("%", colorObj.g)
                    .replace("%", colorObj.b)
                   );
        }//}}}

        function parseColor(color){//{{{
            if (typeof color == "object") 
                return color || colorTable.black;

            if (typeof color == "string")
                return colorTable[color] || colorTable.black;

            return colorTable.black;
        }//}}}

        function updateKeys(event){//{{{
            keyboard.update(event);
            event.preventDefault();
            return false;
        }//}}}

        function updateMouse(event){//{{{
            mouse.update(event, canvas);
            canvas.focus();
            event.preventDefault();
            return false;
        }//}}}

        function windowCheck(){//{{{
            if(window.innerWidth != width || window.innerHeight != height){
                setSize(window.innerWidth, window.innerHeight);
            }
        }//}}}

        // public
        function clear(){//{{{
            if(windowWatch)
                windowCheck();

            ctx.clearRect(0, 0, width, height);
            return this;
        }//}}}

        function rect(x, y, w, h){//{{{
            ctx.fillRect(x, y, w, h);
            return this;
        }//}}}

    function circle(x, y, radius){//{{{
        ctx.beginPath();
        ctx.arc(x, y, radius, Math.PI * 2, false);
        ctx.fill();
        ctx.closePath();
        return this;
    }//}}}

    function text(x, y, text){//{{{
        ctx.textAlign = "left";
        ctx.textBaseline = "top";
        ctx.fillText(text, x, y);
        return this;
    }//}}}

    function centerText(text, offsetX, offsetY){//{{{
        var ox = width / 2  + (offsetX || 0),
            oy = height / 2 + (offsetY || 0);

        ctx.textAlign = "center";
        ctx.textBaseline = "middle";
        ctx.fillText(text, ox, oy);
        return this;
    }//}}}

    function setFont(font){//{{{
        ctx.font = font;
        return this;
    }//}}}

        function setColor(color){//{{{
            ctx.fillStyle = colorToRGB(color);
            return this;
        }//}}}

        function setFullscreen(){ /* TODO */ }

        function setSize(_width, _height){//{{{
            width  = _width;
            height = _height;
            canvas.width  = width;
            canvas.height = height;
            return this;
        }//}}}

        function setStyle(style, value){//{{{
            canvas.style[style] = value;
            return this;
        }//}}}

        function initEvents(){//{{{
            canvas.onkeydown     = updateKeys;
            canvas.onkeyup       = updateKeys;
            canvas.onmousedown   = updateMouse;
            canvas.onmouseup     = updateMouse;
            canvas.oncontextmenu = updateMouse;
            canvas.onmousemove   = updateMouse;
            return this;
        }//}}}

        function setWindowWatch(){//{{{
            windowWatch = true;
            return this;
        }//}}}

        function keyIsDown(key){//{{{
            if(typeof key == "number")
                return keyboard.keyDown(key);

            if(typeof key == "string")
                return keyboard.keyDown(keyTable[key]);

            return false;
        }//}}}

        function mouseLeft(){//{{{
            return mouse.leftDown();
        }//}}}

        function mouseMiddle(){//{{{
            return mouse.middleDown();
        }//}}}

        function mouseRight(){//{{{
            return mouse.rightDown();
        }//}}}

        function mousePosition(){//{{{
            return mouse.getPosition();
        }//}}}

        function getKeyTable(){//{{{
            return keyTable;
        }//}}}

        function getColorTable(){//{{{
            return colorTable;
        }//}}}

        init();
        this.clear          = clear;
        this.rect           = rect;
        this.circle         = circle;
        this.text           = text;
        this.centerText     = centerText;
        this.setFont        = setFont;
        this.setColor       = setColor;
        this.setWindowWatch = setWindowWatch;
        this.setSize        = setSize;
        this.setStyle       = setStyle;
        this.initEvents     = initEvents;
        this.keyIsDown      = keyIsDown;
        this.mouseLeft      = mouseLeft;
        this.mouseMiddle    = mouseMiddle;
        this.mouseRight     = mouseRight;
        this.mousePosition  = mousePosition;
        this.getKeyTable    = getKeyTable;
        this.getColorTable  = getColorTable;
    }//}}}

    return {
        Canvas2D : Canvas2D,
        Color    : Color,
    };

}());
