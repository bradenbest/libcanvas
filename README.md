This is a single-file library that provides some abstraction for the Canvas API.

I created it by combining parts the different "draw.js" and "input.js" modules I've written for past projects.

## Usage:

First, include it

    <script src="canvas.js"></script>

Then, in Javascript:

    var canvas = new Canvas.Canvas2D();

Or, if there's already a canvas in the document:

    var canvas = new Canvas.Canvas2D(document.getElementById("canvas"));

Then, use any of the public functions:

    ([var] means argument is optional)

    chainable (returns 'this'):
        clear(<none>)
        rect(<Number> x, <Number> y, <Number> w, <Number> h)
        circle(<Number> x, <Number> y, <Number> radius)
        text(<Number> x, <Number> y, <String> text)
        centerText(<String> text, <Number> [x], <Number> [y])
        setFont(<String> font)
        setColor(<Color | String> color)
        setSize(<Number> width, <Number> height)
        setStyle(<String> style, <String> value)
        setWindowWatch(<none>)
        initEvents(<none>)

    non-chainable:
        keyIsDown(<String | Number> key) -> true/false
        mouseLeft(<none>)                -> true/false
        mouseMiddle(<none>)              -> true/false
        mouseRight(<none>)               -> true/false
        mousePosition(<none>)            -> <Array> [x, y]
        getKeyTable(<none>)              -> <Object>
        getColorTable(<none>)            -> <Object>

    classes:
        Color(<Number> r, <Number> g, <Number> b)     -> <Color>
        Canvas2D(<HTMLCanvasElement> [canvasElement]) -> <Canvas2D>

I should definitely put that in a wiki page at some point. (TODO)

Anyways, here's an example:

    canvas
    .setSize(100, 100)
    .setColor("red")
    .rect(0, 0, 50, 50)
    .setColor("blue")
    .circle(50, 50, 10)
    .setColor("green")
    .centerText("Hello World");

That's what I meant by chainable. You can chain them like you can with jQuery calls.

For a real-world exmaple, see [here](https://gitlab.com/snippets/14259)
